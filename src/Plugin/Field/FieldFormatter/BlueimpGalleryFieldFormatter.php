<?php

/**
 * @file
 * Contains \Drupal\blueimp_gallery\Plugin\Field\FieldFormatter\BlueimpGalleryFieldFormatter.
 */

namespace Drupal\blueimp_gallery\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'blueimp_gallery_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "blueimp_gallery_field_formatter",
 *   label = @Translation("Blueimp gallery field formatter"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class BlueimpGalleryFieldFormatter extends ImageFormatter {

  protected $imageStyles;

  /**
   * BlueimpGalleryFieldFormatter constructor.
   *
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param string $label
   * @param string $view_mode
   * @param array $third_party_settings
   * @param \Drupal\Core\Session\AccountInterface $current_user
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, \Drupal\Core\Session\AccountInterface $current_user, \Drupal\Core\Entity\EntityStorageInterface $image_style_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage);
    $this->imageStyles = ['source' => $this->t('Source path')] + image_style_options(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'thumb_style' => 'medium',
      'view_style' => 'source',
      'options' => [
        'continuous' => TRUE,
        'start_controls' => TRUE,
        'carousel' => TRUE,
        'start_slideshow' => FALSE,
        'show_indicator' => TRUE,
        'stretch_images' => FALSE,
        'hide_page_scrollbars' => TRUE,
        'toggle_controls_on_return' => TRUE,
        'toggle_slideshow_on_space' => TRUE,
        'enable_keyboard_navigation' => TRUE,
        'close_on_escape' => TRUE,
        'close_on_slide_click' => FALSE,
        'close_on_swipe_up_or_down' => TRUE,
        'disable_scroll' => TRUE,
      ],
      'slideshow_interval' => 5000,
      'transition_speed' => 400,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $settings = $this->getSettings();
    $image_styles = $this->imageStyles;

    $form['thumb_style'] = array(
      '#type' => 'select',
      '#title' => $this->t('Thumbnail image style'),
      '#description' => $this->t('Image style for thumbnail view'),
      '#default_value' => $settings['thumb_style'],
      '#options' => $image_styles,
    );

    $form['view_style'] = array(
      '#type' => 'select',
      '#title' => $this->t('View image style'),
      '#description' => $this->t('Image style for view'),
      '#default_value' => $settings['view_style'],
      '#options' => $image_styles,
    );


    $form['slideshow_interval'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Delay in milliseconds between slides for the automatic slideshow'),
      '#default_value' => $settings['slideshow_interval'],
    );


    $form['transition_speed'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Transition speed'),
      '#default_value' => $settings['transition_speed'],
    );

    $form['options'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Blueimp gallery plugin options'),
      '#description' => $this->t('Show controls on start'),
      '#default_value' => array_keys($settings['options'], TRUE),
      '#options' => array(
        'continuous' => $this->t('Allow continuous navigation, moving from last to first and from first to last slide'),
        'start_controls' => $this->t('Start controls'),
        'carousel' => $this->t('Carousel mode (carousel)'),
        'start_slideshow' => $this->t('Start with the automatic slideshow (startSlideshow)'),
        'show_indicator' => $this->t('Show indicators'),
        'stretch_images' => $this->t('Defines if images should be stretched to fill the available space'),
        'hide_page_scrollbars' => $this->t('Hide the page scrollbars'),
        'toggle_controls_on_return' => $this->t('Toggle the controls on pressing the Return key'),
        'toggle_slideshow_on_space' => $this->t('Toggle the automatic slideshow interval on pressing the Space key'),
        'enable_keyboard_navigation' => $this->t('Navigate the gallery by pressing left and right on the keyboard'),
        'close_on_escape' => $this->t('Close the gallery on pressing the ESC key'),
        'close_on_slide_click' => $this->t('Close the gallery when clicking on an empty slide area'),
        'close_on_swipe_up_or_down' => $this->t('Close the gallery by swiping up or down'),
        'disable_scroll' => $this->t('Stops any touches on the container from scrolling the page'),
      ),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $thumb_style = (isset($settings['thumb_style'])) ? $settings['thumb_style'] : '';

    $summary = [
      '#markup' => $this->t('Thumbnail image style <strong>"!style"</strong>', array(
        '!style' => ($thumb_style) ? $this->imageStyles[$thumb_style] : '',
      )),
      'allowed_tags' => ['strong'],
    ];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $build = [];
    $files = $this->getEntitiesToView($items, $langcode);

    // Passing options to  Blueimp gallery library.
    $settings = $this->getSettings();
    $flags = $settings['options'];

    $blueimp_options = [
      'startControls' => (bool) $flags['start_controls'],
      'hidePageScrollbars' => (bool) $flags['hide_page_scrollbars'],
      'carousel' => (bool) $flags['carousel'],
      'startSlideshow' => (bool) $flags['start_slideshow'],
      'continuous' => (bool) $flags['continuous'],
      'showIndicator' => (bool) $flags['show_indicator'],
      'slideshowInterval' => (int) $settings['slideshow_interval'],
      'transitionSpeed' => (int) $settings['transition_speed'],
      'stretchImages' => (bool) $flags['stretch_images'],
      'toggleControlsOnReturn' => (bool) $flags['toggle_controls_on_return'],
      'toggleSlideshowOnSpace' => (bool) $flags['toggle_slideshow_on_space'],
      'enableKeyboardNavigation' => (bool) $flags['enable_keyboard_navigation'],
      'closeOnEscape' => (bool) $flags['close_on_escape'],
      'closeOnSlideClick' => (bool) $flags['close_on_slide_click'],
      'closeOnSwipeUpOrDown' => (bool) $flags['close_on_swipe_up_or_down'],
      'disableScroll' => (bool) $flags['disable_scroll'],
    ];
    $field_name = $this->fieldDefinition->getName();
    $build['#attached']['drupalSettings']['blueimpGallery'][$field_name] = $blueimp_options;
    $build['#attached']['library'][] = 'blueimp_gallery/gallery';
    $build['#prefix'] = '<div class="blueimp-gallery-items clearfix">';
    $build['#suffix'] = '</div>';

    if (empty($files)) {
      return $build;
    }

    // Prepare items data.
    $url = NULL;
    $link_file = TRUE;
    $image_style_setting = $this->getSetting('thumb_style');

    // Collect cache tags to be added for each item in the field.
    $base_cache_tags = [];
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $base_cache_tags = $image_style->getCacheTags();
    }

    foreach ($files as $delta => $file) {
      $cache_contexts = [];
      if (isset($link_file)) {
        /** @var \Drupal\file\Entity\File $image_uri */
        $image_uri = $file->getFileUri();
        $url = Url::fromUri(file_create_url($image_uri));
        $cache_contexts[] = 'url.site';
      }
      $cache_tags = Cache::mergeTags($base_cache_tags, $file->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $item_attributes['data-gallery-group'] = $field_name;

      $build[$delta] = array(
        '#theme' => 'image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => $image_style_setting,
        '#url' => ($settings['view_style'] == 'source') ? $url : $this->imageStyleStorage->load($settings['view_style'])
                                                                                         ->buildUrl($image_uri),
        '#cache' => array(
          'tags' => $cache_tags,
          'contexts' => $cache_contexts,
        ),
      );
    }

    return $build;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
