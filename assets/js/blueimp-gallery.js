/**
 * @file
 * Provides blueimp gallery wrapper.
 */

(function ($, Drupal, drupalSettings) {

    'use strict';

    var getBlueimpOptions = function (group) {
        return drupalSettings.blueimpGallery[group];
    };

    var getGroupName = function (elem) {
        var group = elem.attr("data-gallery-group");
        return (group !== undefined) ? group : "default";
    };

    // Create blueimp gallery modal.
    var galleryInit = function (data, options) {
        var id = "#blueimp-gallery";
        var wrapper = $(id);
        if (wrapper.length === 0) {
            $("body").append("<div id=\"blueimp-gallery\" class=\"blueimp-gallery\">");
            wrapper = $(id);
            if (options.startControls) {
                wrapper.addClass("blueimp-gallery-controls")
            }
            wrapper.append("<div class=\"slides\" />");
            wrapper.append("<h3 class=\"title\" />");
            wrapper.append("<a class=\"prev\">‹</a>");
            wrapper.append("<a class=\"next\">›</a>");
            wrapper.append("<a class=\"close\">x</a>");
            wrapper.append("<a class=\"play-pause\"></a>");
            if (options.showIndicator) {
                wrapper.append("<ol class=\"indicator\"></ol>");
            }
        }
        blueimp.Gallery(data, options);
    };

    Drupal.behaviors.blueimpGalleryImageFieldFormatter = {
        attach: function (context) {
            var items = $(".blueimp-gallery-items").find("a");
            var dataGroup = {};
            $.each(items, function (ind, val) {
                var group = getGroupName($(val).find("img"));
                dataGroup[group] = dataGroup[group] || [];
                dataGroup[group].push(val);
            });

            items.once().click(
                function () {
                    event.preventDefault();
                    var group = getGroupName($(this).find("img"));
                    var options = getBlueimpOptions(group);
                    var index = $.inArray(this, dataGroup[group]);
                    if (index > -1) {
                        options["index"] = index;
                    }
                    $("#blueimp-gallery").remove();
                    galleryInit(dataGroup[group], options);
                }
            );
        }
    };

})(jQuery, Drupal, drupalSettings);